from __future__ import print_function

from sqlalchemy import create_engine
from cubes.tutorial.sql import create_table_from_csv

FACT_TABLE = "songs"

print("Preparing data...")

engine = create_engine('sqlite:///data.sqlite')

create_table_from_csv(engine,
                      "data.csv",
                      table_name=FACT_TABLE,
                      fields=[
                            ("id", "integer"),
                            ("song", "string"),
                            ("artist", "string"),
                            ("genre", "string"),
                            ("year","integer"),
                            ("date_added","string"),
                            ("bpm", "integer"),
                            ("energy", "integer"),
                            ("danceability", "integer"),
                            ("db", "integer"),
                            ("liveness", "integer"),
                            ("valence", "integer"),
                            ("length", "integer"),
                            ("acousticness", "integer"),
                            ("speechiness", "integer"),
                            ("popularity", "integer"),
                            ("country","string")],
                      create_id=False
                  )

print("File data.sqlite created")

