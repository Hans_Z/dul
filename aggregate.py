from __future__ import print_function
from cubes import Workspace, Cell, PointCut


workspace = Workspace()
workspace.register_default_store("sql", url="sqlite:///data.sqlite")
workspace.import_model("model.json")


browser = workspace.browser("songs")


result = browser.aggregate()

print("Total\n"
      "----------------------------")

print("Record count      : %8d" % result.summary["record_count"])
print("Average bpm       : %8d" % result.summary["bpm_avg"])
print("Average popularity: %8d" % result.summary["popularity_avg"])
print("Total length      : %8d" % result.summary["length_sum"])

print("\n"
      "Drill Down by Genre\n"+"="*76)

result = browser.aggregate(drilldown=["genre"])

print(("%-16s%20s%20s%20s\n"+"-"*76) % ("Genre", "Count", "Average bpm", "Average popularity"))

for row in result.table_rows("genre"):
    print("%-16s%20d%20d%20d" % ( row.label,
                              row.record["record_count"],
                              row.record["bpm_avg"],
                              row.record["popularity_avg"])
                              )

print("\n"
      "Drill Down by Artist\n"+"="*76)

result = browser.aggregate(drilldown=["artist"])

print(("%-16s%20s%20s%20s\n"+"-"*76) % ("Artist", "Count", "Average bpm", "Average popularity"))

for row in result.table_rows("artist"):
    print("%-16s%20d%20d%20d" % ( row.label,
                              row.record["record_count"],
                              row.record["bpm_avg"],
                              row.record["popularity_avg"])
                              )

print("\n"
      "Slice where Artist = Ed Sheeran\n"+
      "="*150)

cut = PointCut("artist", ["Ed Sheeran"])
cell = Cell(browser.cube, cuts = [cut])
result = browser.aggregate(cell,drilldown=["artist"])

print(("%30s%30s%30s%30s%30s\n"+"-"*150) % ("Number of records", "Beats per minute", "Popularity", "Total length", "Average length"))

print("%30s%30s%30s%30s%30s" % (
                        result.summary["record_count"],
                        result.summary["bpm_avg"],
                        result.summary["popularity_avg"],
                        result.summary["length_sum"],
                        result.summary["length_avg"])
                        )

cut = PointCut("country",["africa"])
cell = Cell(browser.cube,cuts=[cut])
result = browser.aggregate(cell,drilldown=["country"])

print("\n"
      "Slice where country = Africa\n"+
      "="*150)

print(("%30s%30s%30s%30s%30s\n"+"-"*150) % ("Number of records", "Beats per minute", "Popularity", "Total length", "Average length"))

print("%30s%30s%30s%30s%30s" % (
                        result.summary["record_count"],
                        result.summary["bpm_avg"],
                        result.summary["popularity_avg"],
                        result.summary["length_sum"],
                        result.summary["length_avg"])
                        )

cut = PointCut("genre",["dance pop"])
cell = Cell(browser.cube,cuts=[cut])
result = browser.aggregate(cell,drilldown=["genre"])

print("\n"
      "Slice where genre = dance pop\n"+
      "="*150)

print(("%30s%30s%30s%30s%30s\n"+"-"*150) % ("Number of records", "Beats per minute", "Popularity", "Total length", "Average length"))

print("%30s%30s%30s%30s%30s" % (
                        result.summary["record_count"],
                        result.summary["bpm_avg"],
                        result.summary["popularity_avg"],
                        result.summary["length_sum"],
                        result.summary["length_avg"])
                        )


cuts = [PointCut("country",["world"]),PointCut("genre",["dance pop"])]
cell = Cell(browser.cube,cuts=cuts)
result = browser.aggregate(cell,drilldown=["country","genre"])

print("\n"
      "Slice where country = world, genre = dance pop\n"+
      "="*150)

print(("%30s%30s%30s%30s%30s\n"+"-"*150) % ("Number of records", "Beats per minute", "Popularity", "Total length", "Average length"))

print("%30s%30s%30s%30s%30s" % (
                        result.summary["record_count"],
                        result.summary["bpm_avg"],
                        result.summary["popularity_avg"],
                        result.summary["length_sum"],
                        result.summary["length_avg"])
                        )